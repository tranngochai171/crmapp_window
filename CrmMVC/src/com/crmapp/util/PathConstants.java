package com.crmapp.util;

public class PathConstants {
	// HOME VIEW
	public static final String PATH_HOME = "/views/home/index.jsp";
	// LOGIN VIEW
	public static final String PATH_LOGIN = "/views/login/index.jsp";
	// ERROR VIEW
	public static final String PATH_403 = "/views/error/403.jsp";
	public static final String PATH_404 = "/views/error/404.jsp";
	// ROLE VIEW
	public static final String PATH_ROLE = "/views/role/index.jsp";
	public static final String PATH_ROLE_ADD = "/views/role/add.jsp";
	public static final String PATH_ROLE_EDIT = "/views/role/edit.jsp";
	// USER VIEW
	public static final String PATH_USER = "/views/user/index.jsp";
	public static final String PATH_USER_ADD = "/views/user/add.jsp";
	public static final String PATH_USER_EDIT = "/views/user/edit.jsp";
	public static final String PATH_USER_DETAIL = "/views/user/detail.jsp";
	// GROUPWORK VIEW
	public static final String PATH_GROUPWORK = "/views/groupwork/index.jsp";
	public static final String PATH_GROUPWORK_ADD = "/views/groupwork/add.jsp";
	public static final String PATH_GROUPWORK_EDIT = "/views/groupwork/edit.jsp";
	public static final String PATH_GROUPWORK_DETAIL = "/views/groupwork/detail.jsp";
	// LAYOUTS VIEW
	public static final String PATH_STYLE = "/views/layouts/style.jsp";
	public static final String PATH_NAVBAR = "/views/layouts/navbar.jsp";
	public static final String PATH_SIDEBAR = "/views/layouts/sidebar.jsp";
	public static final String PATH_SCRIPT = "/views/layouts/script.jsp";
	public static final String PATH_FOOTER = "/views/layouts/footer.jsp";
}
