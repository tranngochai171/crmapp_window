package com.crmapp.util;

public class UrlConstants {
	// HOME CONTROLLER
	public static final String URL_HOME = "/index";
	// LOGIN CONTROLLER
	public static final String URL_LOGIN = "/login";
	// ERROR CONTROLLER
	public static final String URL_403 = "/403";
	public static final String URL_404 = "/404";
	// ROLE CONTROLLER
	public static final String URL_ROLE = "/role-table";
	public static final String URL_ROLE_ADD = "/role-add";
	public static final String URL_ROLE_DELETE = "/role-delete";
	public static final String URL_ROLE_EDIT = "/role-edit";
	// ROLE CONTROLLER
	public static final String URL_USER = "/user-table";
	public static final String URL_USER_ADD = "/user-add";
	public static final String URL_USER_DELETE = "/user-delete";
	public static final String URL_USER_EDIT = "/user-edit";
	public static final String URL_USER_DETAIL = "/user-detail";
	// GROUPWORK CONTROLLER
	public static final String URL_GROUPORK = "/groupwork-table";
	public static final String URL_GROUPORK_ADD = "/groupwork-add";
	public static final String URL_GROUPORK_DELETE = "/groupwork-delete";
	public static final String URL_GROUPORK_EDIT = "/groupwork-edit";
	public static final String URL_GROUPORK_DETAIL = "/groupwork-detail";
}
