package com.crmapp.util;

import javax.servlet.http.HttpServletRequest;

public class CommonMethods {
	public static void setNotification(HttpServletRequest req, String sessionAtributes) {
		if (req.getSession().getAttribute(sessionAtributes) != null) {
			String message = (String) req.getSession().getAttribute(sessionAtributes);
			req.setAttribute(sessionAtributes, message);
			req.getSession().removeAttribute(sessionAtributes);
		}
	}
}
