package com.crmapp.controller;

import java.io.IOException;
import java.sql.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.crmapp.dao.JobDAO;
import com.crmapp.model.Job;
import com.crmapp.util.CommonMethods;
import com.crmapp.util.PathConstants;
import com.crmapp.util.UrlConstants;

@WebServlet(urlPatterns = { UrlConstants.URL_GROUPORK, UrlConstants.URL_GROUPORK_DELETE, UrlConstants.URL_GROUPORK_ADD,
		UrlConstants.URL_GROUPORK_EDIT, UrlConstants.URL_GROUPORK_DETAIL })
public class GroupworkController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JobDAO jobDAO;

	public GroupworkController() {
		this.jobDAO = new JobDAO();
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String action = req.getServletPath();
		switch (action) {
		case UrlConstants.URL_GROUPORK_ADD:
			req.getRequestDispatcher(PathConstants.PATH_GROUPWORK_ADD).forward(req, resp);
			return;
		case UrlConstants.URL_GROUPORK_DELETE:
			if (this.jobDAO.deleteJob(Integer.parseInt(req.getParameter("id"))) <= 0)
				req.setAttribute("fail", "Xóa công việc thất bại");
			else
				req.setAttribute("success", "Xóa công việc thất bại");
			break;
		case UrlConstants.URL_GROUPORK_EDIT:
			Job job = this.jobDAO.findById(Integer.parseInt(req.getParameter("id")));
			req.setAttribute("item", job);
			req.getRequestDispatcher(PathConstants.PATH_GROUPWORK_EDIT).forward(req, resp);
			return;
		case UrlConstants.URL_GROUPORK_DETAIL:
			req.getRequestDispatcher(PathConstants.PATH_GROUPWORK_DETAIL).forward(req, resp);
			return;
		}
		CommonMethods.setNotification(req, "success");
		CommonMethods.setNotification(req, "fail");
		List<Job> listJob = this.jobDAO.getListJob();
		req.setAttribute("listJob", listJob);
		req.getRequestDispatcher(PathConstants.PATH_GROUPWORK).forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String action = req.getServletPath();
		String name = req.getParameter("name");
		Date start_date = Date.valueOf(req.getParameter("start_date"));
		Date end_date = Date.valueOf(req.getParameter("end_date"));
		switch (action) {
		case UrlConstants.URL_GROUPORK_ADD: {
			Job job = new Job(name, start_date, end_date);
			if (this.jobDAO.addNewJob(job) <= 0)
				req.getSession().setAttribute("fail", "Thêm mối công việc thất bại");
			else
				req.getSession().setAttribute("success", "Thêm mối công việc thành công");
		}
			break;
		case UrlConstants.URL_GROUPORK_EDIT:
			int id = Integer.parseInt(req.getParameter("id"));
			Job job = new Job(id, name, start_date, end_date);
			if (this.jobDAO.editJob(job) <= 0)
				req.getSession().setAttribute("fail", "Sửa công việc thất bại");
			else
				req.getSession().setAttribute("success", "Sửa công việc thành công");
			break;
		}
		resp.sendRedirect(req.getContextPath() + UrlConstants.URL_GROUPORK);
	}
}
