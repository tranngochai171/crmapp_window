package com.crmapp.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.crmapp.util.PathConstants;
import com.crmapp.util.UrlConstants;

@WebServlet(urlPatterns = { UrlConstants.URL_403, UrlConstants.URL_404 })
public class ErrorController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String action = req.getServletPath();
		switch (action) {
		case UrlConstants.URL_403:
			req.getRequestDispatcher(PathConstants.PATH_403).forward(req, resp);
			return;
		case UrlConstants.URL_404:
			req.getRequestDispatcher(PathConstants.PATH_404).forward(req, resp);
			return;
		}
	}
}
