package com.crmapp.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.crmapp.dao.RoleDAO;
import com.crmapp.model.Role;
import com.crmapp.util.CommonMethods;
import com.crmapp.util.PathConstants;
import com.crmapp.util.UrlConstants;

@WebServlet(urlPatterns = { UrlConstants.URL_ROLE, UrlConstants.URL_ROLE_ADD, UrlConstants.URL_ROLE_DELETE,
		UrlConstants.URL_ROLE_EDIT })
public class RoleController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private RoleDAO roleDAO = null;

	public RoleController() {
		this.roleDAO = new RoleDAO();
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String action = req.getServletPath();
		switch (action) {
//		case UrlConstants.URL_ROLE:
//			break;
		case UrlConstants.URL_ROLE_ADD:
			req.getRequestDispatcher(PathConstants.PATH_ROLE_ADD).forward(req, resp);
			return;
		case UrlConstants.URL_ROLE_DELETE:
			int id_delete = Integer.parseInt(req.getParameter("id"));
			if (this.roleDAO.deleteRole(id_delete) <= 0) {
				req.setAttribute("fail", "Xóa quyền thất bại");
			} else {
				req.setAttribute("success", "Xóa quyền thành công");
			}
			break;
		case UrlConstants.URL_ROLE_EDIT:
			int id = Integer.parseInt(req.getParameter("id"));
			Role role = this.roleDAO.findById(id);
			req.setAttribute("item", role);
			req.getRequestDispatcher(PathConstants.PATH_ROLE_EDIT).forward(req, resp);
			return;
		}
		List<Role> listRole = this.roleDAO.getListRole();
		req.setAttribute("listRole", listRole);
		CommonMethods.setNotification(req, "success");
		CommonMethods.setNotification(req, "fail");
		req.getRequestDispatcher(PathConstants.PATH_ROLE).forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String action = req.getServletPath();
		String name = req.getParameter("name").trim();
		String description = req.getParameter("description").trim();
		switch (action) {
		case UrlConstants.URL_ROLE_ADD: {
			Role role = new Role(name, description);
			if (this.roleDAO.addNewRole(role) <= 0) {
				req.getSession().setAttribute("fail", "Thêm mới quyền thất bại");
			} else {
				req.getSession().setAttribute("success", "Thêm mới quyền thành công");
			}
			break;
		}
		case UrlConstants.URL_ROLE_EDIT:
			Role role = new Role(Integer.parseInt(req.getParameter("id")), name, description);
			if (this.roleDAO.editRole(role) <= 0) {
				req.getSession().setAttribute("fail", "Sửa quyền thành công");
			} else {
				req.getSession().setAttribute("success", "Sửa quyền thành công");
			}
			break;
		}
		resp.sendRedirect(req.getContextPath() + UrlConstants.URL_ROLE);
	}

//	private void setNotification(HttpServletRequest req, String sessionAtributes) {
//		if (req.getSession().getAttribute(sessionAtributes) != null) {
//			String message = (String) req.getSession().getAttribute(sessionAtributes);
//			req.setAttribute(sessionAtributes, message);
//			req.getSession().removeAttribute(sessionAtributes);
//		}
//	}
}
