package com.crmapp.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mindrot.jbcrypt.BCrypt;

import com.crmapp.dao.UserDAO;
import com.crmapp.dto.UserDto;
import com.crmapp.util.PathConstants;
import com.crmapp.util.UrlConstants;

@WebServlet(urlPatterns = { UrlConstants.URL_LOGIN })
public class LoginController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private UserDAO userDAO;

	public LoginController() {
		this.userDAO = new UserDAO();
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if (req.getParameter("action") != null && req.getParameter("action").equals("logout"))
			req.getSession().removeAttribute("USER");
		req.getRequestDispatcher(PathConstants.PATH_LOGIN).forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String email = req.getParameter("email").trim();
		String password = req.getParameter("password").trim();
		UserDto user_check = this.userDAO.findByEmail(email);
		if (user_check == null || !BCrypt.checkpw(password, user_check.getPassword())) {
			req.setAttribute("message", "(*) Thông tin đăng nhập chưa chính xác");
			req.getRequestDispatcher(PathConstants.PATH_LOGIN).forward(req, resp);
			return;
		}
		req.getSession().setAttribute("USER", user_check);
		resp.sendRedirect(req.getContextPath() + UrlConstants.URL_HOME);
	}
}
