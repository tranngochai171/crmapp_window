package com.crmapp.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mindrot.jbcrypt.BCrypt;

import com.crmapp.dao.RoleDAO;
import com.crmapp.dao.UserDAO;
import com.crmapp.dto.UserDto;
import com.crmapp.model.Role;
import com.crmapp.model.User;
import com.crmapp.util.CommonMethods;
import com.crmapp.util.PathConstants;
import com.crmapp.util.UrlConstants;

@WebServlet(urlPatterns = { UrlConstants.URL_USER, UrlConstants.URL_USER_ADD, UrlConstants.URL_USER_DELETE,
		UrlConstants.URL_USER_EDIT, UrlConstants.URL_USER_DETAIL })
public class UserController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private UserDAO userDAO;
	private RoleDAO roleDAO;

	public UserController() {
		this.userDAO = new UserDAO();
		this.roleDAO = new RoleDAO();
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String action = req.getServletPath();
		switch (action) {
		case UrlConstants.URL_USER_ADD: {
			List<Role> listRole = this.roleDAO.getListRole();
			req.setAttribute("listRole", listRole);
			req.getRequestDispatcher(PathConstants.PATH_USER_ADD).forward(req, resp);
		}
			return;
		case UrlConstants.URL_USER_DELETE:
			if (this.userDAO.deleteUser(Integer.parseInt(req.getParameter("id"))) <= 0)
				req.setAttribute("fail", "Xóa thành viên thất bại");
			else
				req.setAttribute("success", "Xóa thành viên thành công");
			break;
		case UrlConstants.URL_USER_EDIT:
			if (this.userDAO.findById(Integer.parseInt(req.getParameter("id"))) != null) {
				User user = this.userDAO.findById(Integer.parseInt(req.getParameter("id")));
				req.setAttribute("item", user);
				List<Role> listRole = this.roleDAO.getListRole();
				req.setAttribute("listRole", listRole);
				req.getRequestDispatcher(PathConstants.PATH_USER_EDIT).forward(req, resp);
				return;
			}
			break;
		case UrlConstants.URL_USER_DETAIL:
			if (req.getParameter("id") != null) {
				req.setAttribute("item", this.userDAO.findById(Integer.parseInt(req.getParameter("id"))));
				req.getRequestDispatcher(PathConstants.PATH_USER_DETAIL).forward(req, resp);
				return;
			}
		}
		CommonMethods.setNotification(req, "success");
		CommonMethods.setNotification(req, "fail");
		List<UserDto> listUserDto = this.userDAO.getListUserDto();
		req.setAttribute("listUser", listUserDto);
		req.getRequestDispatcher(PathConstants.PATH_USER).forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String action = req.getServletPath().trim();
		int role_id = Integer.parseInt(req.getParameter("role_id").trim());
		String fullname = req.getParameter("fullname").trim();
		String email = req.getParameter("email").trim();
		switch (action) {
		case UrlConstants.URL_USER_ADD: {
			String password = req.getParameter("password");
			String hashed = BCrypt.hashpw(password, BCrypt.gensalt(12));
			User user = new User(email, hashed, fullname, role_id);
			if (this.userDAO.addNewUser(user) <= 0)
				req.getSession().setAttribute("fail", "Thêm mới thành viên thất bại");
			else
				req.getSession().setAttribute("success", "Thêm mới thành viên thành cồng");
		}
			break;
		case UrlConstants.URL_USER_EDIT:
			if (!this.checkPassword(req)) {
				User user = this.userDAO.findById(Integer.parseInt(req.getParameter("id")));
				req.setAttribute("item", user);
				List<Role> listRole = this.roleDAO.getListRole();
				req.setAttribute("listRole", listRole);
				req.getRequestDispatcher(PathConstants.PATH_USER_EDIT).forward(req, resp);
				return;
			} else {
				String password = req.getParameter("newpassword");
				User user = new User(Integer.parseInt(req.getParameter("id")),
						BCrypt.hashpw(password, BCrypt.gensalt(12)), fullname, role_id);
				if (this.userDAO.editUser(user) <= 0)
					req.getSession().setAttribute("fail", "Sửa thông tin thành viên thất bại");
				else
					req.getSession().setAttribute("success", "Sửa thông tin thành viên thành công");
			}
		}
		resp.sendRedirect(req.getContextPath() + UrlConstants.URL_USER);
	}

	private boolean checkPassword(HttpServletRequest req) {
		String oldpassword = req.getParameter("oldpassword").trim();
		String newpassword = req.getParameter("newpassword").trim();
		int id = Integer.parseInt(req.getParameter("id"));
		User user = this.userDAO.findById(id);
		if (!BCrypt.checkpw(oldpassword, user.getPassword())) {
			req.setAttribute("message", "(*)Mật khẩu cũ chưa đúng");
			return false;
		} else if (oldpassword.equals(newpassword)) {
			req.setAttribute("message", "(*)Mật khẩu cũ trùng với mật khẩu mới");
			return false;
		}
		return true;
	}
//	private void setNotification(HttpServletRequest req, String sessionAtributes) {
//		if (req.getSession().getAttribute(sessionAtributes) != null) {
//			String message = (String) req.getSession().getAttribute(sessionAtributes);
//			req.setAttribute(sessionAtributes, message);
//			req.getSession().removeAttribute(sessionAtributes);
//		}
//	}
}
