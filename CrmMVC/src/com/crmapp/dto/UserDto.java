package com.crmapp.dto;

public class UserDto {
	private int id;
	private String email;
	private String password;
	private String fullname;
	private int role_id;
	private String role_name;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getRole_id() {
		return role_id;
	}

	public void setRole_id(int role_id) {
		this.role_id = role_id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getRole_name() {
		return role_name;
	}

	public void setRole_name(String role_name) {
		this.role_name = role_name;
	}

	public UserDto() {

	}

	public UserDto(int id, String email, String fullname, String role_name) {
		this.id = id;
		this.email = email;
		this.fullname = fullname;
		this.role_name = role_name;
	}
}
