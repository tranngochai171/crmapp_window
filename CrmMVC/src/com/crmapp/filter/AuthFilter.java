package com.crmapp.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.crmapp.dto.UserDto;
import com.crmapp.util.UrlConstants;

@WebFilter(urlPatterns = "/*")
public class AuthFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		request.setCharacterEncoding("UTF-8");
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		String action = req.getServletPath();
		if (action.startsWith(UrlConstants.URL_LOGIN)) {
			chain.doFilter(request, response);
			return;
		}
		UserDto user_check = (UserDto) req.getSession().getAttribute("USER");
		if (user_check != null) {
			if (user_check.getRole_name().equals("ROLE_LEADER")) {
				if (action.startsWith(UrlConstants.URL_ROLE)) {
					resp.sendRedirect(req.getContextPath() + UrlConstants.URL_403);
					return;
				}
			} else if (user_check.getRole_name().equals("ROLE_USER")) {
				if (action.startsWith(UrlConstants.URL_ROLE) || action.startsWith(UrlConstants.URL_USER)) {
					resp.sendRedirect(req.getContextPath() + UrlConstants.URL_403);
					return;
				}
			}
			chain.doFilter(request, response);
		} else
			resp.sendRedirect(req.getContextPath() + UrlConstants.URL_LOGIN);
		response.setCharacterEncoding("UTF-8");
	}

}
