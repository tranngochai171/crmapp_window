package com.crmapp.model;

public class User {
	private int id;
	private String email;
	private String password;
	private String fullname;
	private int role_id;
	private String role_name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public int getRole_id() {
		return role_id;
	}

	public void setRole_id(int role_id) {
		this.role_id = role_id;
	}

	public String getRole_name() {
		return role_name;
	}

	public void setRole_name(String role_name) {
		this.role_name = role_name;
	}

	public User() {

	}

	public User(String email, String password) {
		this.email = email;
		this.password = password;
	}

	public User(int id, String password, String fullname, int role_id) {
		this.id = id;
		this.password = password;
		this.fullname = fullname;
		this.role_id = role_id;
	}

	public User(String email, String password, String fullname, int role_id) {
		this.email = email;
		this.password = password;
		this.fullname = fullname;
		this.role_id = role_id;
	}

	public User(int id, String email, String password, String fullname, int role_id, String role_name) {
		super();
		this.id = id;
		this.email = email;
		this.password = password;
		this.fullname = fullname;
		this.role_id = role_id;
		this.role_name = role_name;
	}
}
