package com.crmapp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

import com.crmapp.connection.JDBCConnection;
import com.crmapp.dto.UserDto;
import com.crmapp.model.User;

public class UserDAO {
	public int addNewUser(User user) {
		int result = 0;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "INSERT INTO users (fullname,email,password,role_id) VALUES (?,?,?,?);";
			PreparedStatement statement = conn.prepareStatement(query);
			statement.setString(1, user.getFullname());
			statement.setString(2, user.getEmail());
			statement.setString(3, user.getPassword());
			statement.setInt(4, user.getRole_id());
			result = statement.executeUpdate();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public List<UserDto> getListUserDto() {
		List<UserDto> listUserDto = new LinkedList<UserDto>();
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT * FROM users join roles on users.role_id = roles.id";
			PreparedStatement statement = conn.prepareStatement(query);
			ResultSet res = statement.executeQuery();
			while (res.next()) {
				UserDto userDto = new UserDto();
				userDto.setId(res.getInt("id"));
				userDto.setEmail(res.getString("email"));
				userDto.setFullname(res.getString("fullname"));
				userDto.setRole_name(res.getString("name"));
				listUserDto.add(userDto);
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listUserDto;
	}

	public User checkLogin(User user_check) {
		User user = null;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT * FROM users WHERE email = ? AND password = ?;";
			PreparedStatement statement = conn.prepareStatement(query);
			statement.setString(1, user_check.getEmail());
			statement.setString(2, user_check.getPassword());
			ResultSet res = statement.executeQuery();
			while (res.next()) {
				user = new User();
				user.setEmail(res.getString("email"));
				user.setPassword(res.getString("password"));
				break;
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;
	}

	public User findById(int id) {
		User user = null;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT * FROM users join roles on users.role_id = roles.id where users.id = ?;";
			PreparedStatement statement = conn.prepareStatement(query);
			statement.setInt(1, id);
			ResultSet res = statement.executeQuery();
			if (res.next()) {
				user = new User();
				user.setId(res.getInt("id"));
				user.setEmail(res.getString("email"));
				user.setPassword(res.getString("password"));
				user.setFullname(res.getString("fullname"));
				user.setRole_name(res.getString("name"));
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;
	}

	public int deleteUser(int id) {
		int result = 0;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "DELETE FROM users WHERE id = ?;";
			PreparedStatement statement = conn.prepareStatement(query);
			statement.setInt(1, id);
			result = statement.executeUpdate();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public int editUser(User user) {
		int result = 0;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "UPDATE users SET fullname = ?, role_id = ?, password = ? WHERE id = ?;";
			PreparedStatement statement = conn.prepareStatement(query);
			statement.setString(1, user.getFullname());
			statement.setInt(2, user.getRole_id());
			statement.setString(3, user.getPassword());
			statement.setInt(4, user.getId());
			result = statement.executeUpdate();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public UserDto findByEmail(String email) {
		UserDto userDto = null;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT * FROM users JOIN roles ON users.role_id = roles.id WHERE email = ?;";
			PreparedStatement statement = conn.prepareStatement(query);
			statement.setString(1, email);
			ResultSet res = statement.executeQuery();
			if (res.next()) {
				userDto = new UserDto();
				userDto.setId(res.getInt("id"));
				userDto.setEmail(res.getString("email"));
				userDto.setFullname(res.getString("fullname"));
				userDto.setPassword(res.getString("password"));
				userDto.setRole_id(res.getInt("role_id"));
				userDto.setRole_name(res.getString("name"));
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userDto;
	}
}
