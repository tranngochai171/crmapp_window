package com.crmapp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

import com.crmapp.connection.JDBCConnection;
import com.crmapp.model.Job;

public class JobDAO {
	public int addNewJob(Job job) {
		int result = 0;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "INSERT INTO jobs (name,start_date,end_date) VALUES (?,?,?);";
			PreparedStatement statement = conn.prepareStatement(query);
			statement.setString(1, job.getName());
			statement.setDate(2, job.getStart_date());
			statement.setDate(3, job.getEnd_date());
			result = statement.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public List<Job> getListJob() {
		List<Job> listJob = new LinkedList<Job>();
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT * FROM jobs;";
			PreparedStatement statement = conn.prepareStatement(query);
			ResultSet res = statement.executeQuery();
			while (res.next()) {
				Job job = new Job();
				job.setId(res.getInt("id"));
				job.setName(res.getString("name"));
				job.setStart_date(res.getDate("start_date"));
				job.setEnd_date(res.getDate("end_date"));
				listJob.add(job);
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listJob;
	}

	public int deleteJob(int id) {
		int result = 0;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "DELETE FROM jobs WHERE id = ?;";
			PreparedStatement statement = conn.prepareStatement(query);
			statement.setInt(1, id);
			result = statement.executeUpdate();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public Job findById(int id) {
		Job job = null;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT * FROM jobs WHERE id = ?;";
			PreparedStatement statement = conn.prepareStatement(query);
			statement.setInt(1, id);
			ResultSet res = statement.executeQuery();
			if (res.next()) {
				job = new Job();
				job.setId(res.getInt("id"));
				job.setName(res.getString("name"));
				job.setStart_date(res.getDate("start_date"));
				job.setEnd_date(res.getDate("end_date"));
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return job;
	}

	public int editJob(Job job) {
		int result = 0;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "UPDATE jobs SET name = ?, start_date = ?, end_date = ? WHERE id = ?;";
			PreparedStatement statement = conn.prepareStatement(query);
			statement.setString(1, job.getName());
			statement.setDate(2, job.getStart_date());
			statement.setDate(3, job.getEnd_date());
			statement.setInt(4, job.getId());
			result = statement.executeUpdate();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
}
