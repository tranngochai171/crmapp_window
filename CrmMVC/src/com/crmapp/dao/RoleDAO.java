package com.crmapp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

import com.crmapp.connection.JDBCConnection;
import com.crmapp.model.Role;

public class RoleDAO {
	public List<Role> getListRole() {
		List<Role> listRole = new LinkedList<Role>();
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT * FROM roles;";
			PreparedStatement statement = conn.prepareStatement(query);
			ResultSet res = statement.executeQuery();
			while (res.next()) {
				int id = res.getInt("id");
				String name = res.getString("name");
				String description = res.getString("description");
				Role role = new Role(id, name, description);
				listRole.add(role);
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listRole;
	}

	public int addNewRole(Role role) {
		int result = 0;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "INSERT INTO roles (name,description) VALUES (?,?);";
			PreparedStatement statement = conn.prepareStatement(query);
			statement.setString(1, role.getName());
			statement.setString(2, role.getDescription());
			result = statement.executeUpdate();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public int deleteRole(int id_delete) {
		int result = 0;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "DELETE FROM roles WHERE id = ?;";
			PreparedStatement statement = conn.prepareStatement(query);
			statement.setInt(1, id_delete);
			result = statement.executeUpdate();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public int editRole(Role role) {
		int result = 0;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "UPDATE roles SET name = ?, description = ? WHERE id = ?;";
			PreparedStatement statement = conn.prepareStatement(query);
			statement.setString(1, role.getName());
			statement.setString(2, role.getDescription());
			statement.setInt(3, role.getId());
			result = statement.executeUpdate();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public Role findById(int id) {
		Role role = null;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT * FROM roles WHERE id = ?;";
			PreparedStatement statement = conn.prepareStatement(query);
			statement.setInt(1, id);
			ResultSet res = statement.executeQuery();
			System.out.println(123);
			if (res.next()) {
				role = new Role();
				role.setId(res.getInt("id"));
				role.setName(res.getString("name"));
				role.setDescription(res.getString("description"));
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return role;
	}
}
