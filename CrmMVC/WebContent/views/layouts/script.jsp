<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!-- jQuery -->
<script src="assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="assets/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script
	src="assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="assets/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="assets/js/waves.js"></script>
<!--Counter js -->
<script
	src="assets/plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
<script
	src="assets/plugins/bower_components/counterup/jquery.counterup.min.js"></script>
<!--Morris JavaScript -->
<script src="assets/plugins/bower_components/raphael/raphael-min.js"></script>
<script src="assets/plugins/bower_components/morrisjs/morris.js"></script>
<!-- Custom Theme JavaScript -->
<script src="assets/js/custom.min.js"></script>
<script src="assets/js/dashboard1.js"></script>
<script
	src="assets/plugins/bower_components/toast-master/js/jquery.toast.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
	var successfulNotify = document.getElementById("successfulNotify").value;
	var failedNotify = document.getElementById("failedNotify").value;
	// Kiem tra neu co message
	if (successfulNotify !== null && successfulNotify.length > 0) {
		swal("Thông báo!", successfulNotify, "success");
	}
	if (failedNotify !== null && failedNotify.length > 0) {
		swal("Thông báo!", failedNotify, "error");
	}
</script>