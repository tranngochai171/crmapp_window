<%@page import="com.crmapp.util.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<head>
<title>USER ADD</title>
</head>
<div class="container-fluid">
	<div class="row bg-title">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h4 class="page-title">Sửa thông tin thành viên</h4>
			<c:if test="${message != null}">
				<p class="text-danger">${message }</p>
			</c:if>
		</div>
	</div>
	<!-- /.row -->
	<!-- .row -->
	<div class="row">
		<div class="col-md-2 col-12"></div>
		<div class="col-md-8 col-xs-12">
			<div class="white-box">
				<form class="form-horizontal form-material"
					action='<c:url value="<%=UrlConstants.URL_USER_EDIT%>"/>'
					method="POST">
					<input type="hidden" class="form-control form-control-line"
						name="id" readonly="readonly" value="${item.id }">
					<div class="form-group">
						<label for="example-email" class="col-md-12">Email</label>
						<div class="col-md-12">
							<input type="email" class="form-control form-control-line"
								name="email" id="example-email" readonly="readonly"
								value="${item.email }">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Password cũ</label>
						<div class="col-md-12">
							<input type="password" class="form-control form-control-line"
								required="required" name="oldpassword">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Password mới</label>
						<div class="col-md-12">
							<input type="password" class="form-control form-control-line"
								required="required" name="newpassword">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Full Name</label>
						<div class="col-md-12">
							<input type="text" placeholder="Johnathan Doe"
								class="form-control form-control-line" name="fullname"
								required="required" value="${item.fullname }">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-12">Select Role</label>
						<div class="col-sm-12">
							<select class="form-control form-control-line" name="role_id">
								<c:forEach var="item" items="${listRole }">
									<option value="${item.id }">${item.name }</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-12">
							<button type="submit" class="btn btn-success">Add User</button>
							<a href='<c:url value="<%=UrlConstants.URL_USER%>"/>'
								class="btn btn-primary">Quay lại</a>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="col-md-2 col-12"></div>
	</div>
</div>